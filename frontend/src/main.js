import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import PrimeVue from "primevue/config";
import ConfirmationService from "primevue/confirmationservice";

import "./assets/main.css";
import "primevue/resources/themes/aura-dark-indigo/theme.css";
import "primeicons/primeicons.css";

const app = createApp(App);
app.use(router);
app.use(PrimeVue);
app.use(ConfirmationService);

app.mount("#app");
