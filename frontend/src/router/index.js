import { createRouter, createWebHistory } from "vue-router";

import HomeView from "../views/HomeView.vue";
import FactoryDetailsView from "../views/FactoryDetailsView.vue";
import LoginView from "../views/LoginView.vue";
import FactoryEmployeesView from "../views/FactoryEmployeesView.vue";
import ProfileView from "../views/ProfileView.vue";
import CartView from "../views/CartView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Početna",
      component: HomeView,
    },
    {
      path: "/factories/:id",
      name: "Factory",
      component: FactoryDetailsView,
    },
    {
      path: "/factories/:id/employees",
      name: "Employees",
      component: FactoryEmployeesView,
    },
    {
      path: "/login",
      name: "Login",
      component: LoginView,
    },
    {
      path: "/profile",
      name: "Profil",
      component: ProfileView,
    },
    {
      path: "/cart",
      name: "Korpa",
      component: CartView,
    },
  ],
});

export default router;
