import { ref, computed } from "vue";
import axios from "axios";

axios.defaults.withCredentials = true;
export const session = ref({});

export const getSession = () => {
  return axios
    .get("http://localhost:8080/auth/getSession")
    .then((response) => {
      session.value = response.data.session;
      return response.data.session;
    })
    .catch((error) => {
      // Handle error if necessary
      console.error(error);
      return undefined; // Return undefined or handle error response
    });
};

export const sessionRole = computed(() => {
  return session.value ? session.value.role : undefined;
});

export const isLoggedIn = computed(() => sessionRole.value !== undefined);

export const isAdministrator = computed(
  () => sessionRole.value === "administrator"
);

export const isManager = computed(() => sessionRole.value === "manager");

export const isCustomer = computed(() => sessionRole.value === "customer");

export const userFactoryId = computed(() => {
  return session.value ? session.value.factory : undefined;
});
