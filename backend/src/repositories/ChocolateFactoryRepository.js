import path from "path";
import { readFile, writeFile } from "../utils/jsonFileUtils.js";
import ChocolateFactory from "../models/ChocolateFactory.js";
import Chocolate from "../models/Chocolate.js";
import Comment from "../models/Comment.js";

const factoriesFilePath = path.join(process.cwd(), "/data/factories.json");

class ChocolateFactoryRepository {
  /**
   * Generates a new unique identifier for a factory.
   * @returns {Promise<string>} A promise that resolves to a string representing the generated factory ID.
   */
  static async generateFactoryId() {
    try {
      const entities = await this.getAllFactories();
      const lastEntity = entities[entities.length - 1];
      const lastId = lastEntity ? parseInt(lastEntity._id) : 0;
      return (lastId + 1).toString();
    } catch (error) {
      console.error("Error generating factory ID:", error);
      throw new Error("Could not generate factory ID");
    }
  }

  /**
   * Generates a new unique identifier for a chocolate.
   * @param {string} factoryId - The factory ID.
   * @returns {Promise<string>} A promise that resolves to a string representing the generated chocolate ID.
   */
  static async generateChocolateId(factoryId) {
    try {
      const entities = await this.getAllChocolatesForFactory(factoryId);
      const lastEntity = entities[entities.length - 1];
      const lastId = lastEntity ? parseInt(lastEntity._id.split("_")[1]) : 0;
      return `${factoryId}_${(lastId + 1).toString()}`;
    } catch (error) {
      console.error("Error generating chocolate ID:", error);
      throw new Error("Could not generate chocolate ID");
    }
  }

  /**
   * Gets all ChocolateFactory objects that are not deleted.
   * @returns {Promise<ChocolateFactory[]>} Promise of an Array of ChocolateFactory objects that are not deleted.
   */
  static async getAllFactories() {
    try {
      const data = await readFile(factoriesFilePath);
      return data
        .map((factoryJSON) => ChocolateFactory.fromJSON(factoryJSON))
        .filter((factory) => !factory.deleted);
    } catch (error) {
      console.error("Error getting all factories:", error);
      throw new Error("Could not retrieve factories");
    }
  }

  /**
   * Gets all chocolates within a factory.
   * @param {string} factoryId - Id of a factory.
   * @returns {Promise<Chocolate[]>} Promise of an array of Chocolate objects.
   */
  static async getAllChocolatesForFactory(factoryId) {
    try {
      const factory = await this.getFactoryById(factoryId);
      return factory
        ? factory.chocolates.filter((chocolate) => !chocolate.deleted)
        : [];
    } catch (error) {
      console.error(
        `Error getting chocolates for factory ${factoryId}:`,
        error
      );
      throw new Error("Could not retrieve chocolates for factory");
    }
  }

  /**
   * Retrieves a chocolate factory by its ID.
   * @param {string} factoryId - The ID of the factory to be retrieved.
   * @returns {Promise<ChocolateFactory | undefined>} A promise that resolves to the chocolate factory object if found, or undefined if not found.
   */
  static async getFactoryById(factoryId) {
    try {
      const factories = await this.getAllFactories();
      return factories.find((factory) => factory._id === factoryId);
    } catch (error) {
      console.error(`Error getting factory by ID ${factoryId}:`, error);
      throw new Error("Could not retrieve factory by ID");
    }
  }

  /**
   * Gets a comment by its id.
   *
   * @param {string} commentId
   * @returns {Promise<Comment|undefined>}
   */
  static async getCommentById(commentId) {
    const factories = await this.getAllFactories();

    for (const factory of factories) {
      for (const comment of factory.comments) {
        if (comment._id === commentId) {
          return comment;
        }
      }
    }

    return undefined;
  }

  /**
   * Gets a chocolate by its ID.
   * @param {string} chocolateId
   * @returns {Promise<Chocolate|undefined>}
   */
  static async getChocolateById(chocolateId) {
    try {
      const factoryId = chocolateId.split("_")[0];
      const factory = await this.getFactoryById(factoryId);
      if (!factory) return undefined;
      return factory.chocolates.find(
        (chocolate) => chocolate._id === chocolateId
      );
    } catch (error) {
      console.error(`Error getting chocolate by ID ${chocolateId}:`, error);
      throw new Error("Could not retrieve chocolate by ID");
    }
  }

  /**
   * Updates an existing chocolate factory.
   * @param {ChocolateFactory} factory - The factory object to be updated.
   * @returns {Promise<ChocolateFactory|undefined>} - Updated factory.
   */
  static async updateFactory(factory) {
    try {
      const factories = await this.getAllFactories();
      const index = factories.findIndex((f) => f._id === factory._id);

      if (index === -1) return undefined;

      factories[index] = factory;
      await writeFile(
        factoriesFilePath,
        factories.map((f) => f.toJSON())
      );
      return factory;
    } catch (error) {
      console.error("Error updating factory:", error);
      throw new Error("Could not update factory");
    }
  }

  /**
   * Updates a chocolate.
   * Fields _id and _factoryId have to be set.
   * @param {Chocolate} chocolate - A Chocolate object.
   * @returns {Promise<Chocolate|undefined>}
   */
  static async updateChocolate(chocolate) {
    try {
      const factory = await this.getFactoryById(chocolate._factoryId);
      if (!factory) return undefined;

      const chocolates = factory.chocolates;
      const index = chocolates.findIndex((c) => c._id === chocolate._id);

      if (index === -1) return undefined;

      chocolates[index] = chocolate;
      factory.chocolates = chocolates;

      const updatedFactory = await this.updateFactory(factory);
      return updatedFactory ? chocolate : undefined;
    } catch (error) {
      console.error("Error updating chocolate:", error);
      throw new Error("Could not update chocolate");
    }
  }

  /**
   *
   * @param {Comment} comment
   * @returns {Promise<Comment|undefined>}
   */
  static async updateComment(comment) {
    try {
      const factory = await this.getFactoryById(comment.factory);
      if (!factory) return undefined;

      const comments = factory.comments;
      const index = comments.findIndex((c) => c._id === comment._id);

      if (index === -1) return undefined;

      comments[index] = comment;
      factory.comments = comments;

      const updatedFactory = await this.updateFactory(factory);
      return updatedFactory ? comment : undefined;
    } catch (error) {
      console.error("Error updating comment:", error);
      throw new Error("Could not update comment");
    }
  }

  /**
   * Saves a new chocolate factory.
   * @param {ChocolateFactory} factory - The factory object to be saved.
   * @returns {Promise<void>} A promise that resolves when the factory is successfully saved.
   */
  static async saveFactory(factory) {
    try {
      const factories = await this.getAllFactories();
      factory._id = await this.generateFactoryId();
      factories.push(factory);
      await writeFile(
        factoriesFilePath,
        factories.map((f) => f.toJSON())
      );
      return factory;
    } catch (error) {
      console.error("Error saving factory:", error);
      throw new Error("Could not save factory");
    }
  }

  /**
   * Saves a comment to a factory.
   *
   * @param {Comment} comment
   * @param {ChocolateFactory} factory
   * @returns
   */
  static async saveComment(comment, factory) {
    try {
      comment._id = (+new Date()).toString(36).slice(-8);
      factory.comments.push(comment);
      const updatedFactory = await this.updateFactory(factory);
      return updatedFactory ? comment : undefined;
    } catch (error) {
      console.error("Error saving comment:", error);
      throw new Error("Could not save comment");
    }
  }

  /**
   * Saves a chocolate to a factory.
   * @param {Chocolate} chocolate - A Chocolate object.
   * @param {ChocolateFactory} factory - A Factory object.
   * @returns {Promise<Chocolate|undefined>} A promise of a Chocolate object if successful.
   */
  static async saveChocolate(chocolate, factory) {
    try {
      chocolate._id = await this.generateChocolateId(factory._id);
      factory.chocolates.push(chocolate);
      const updatedFactory = await this.updateFactory(factory);
      return updatedFactory ? chocolate : undefined;
    } catch (error) {
      console.error("Error saving chocolate:", error);
      throw new Error("Could not save chocolate");
    }
  }

  /**
   * Logically deletes a factory.
   * @param {ChocolateFactory} factory
   * @returns {Promise<boolean>}
   */
  static async deleteFactory(factory) {
    try {
      factory.deleted = true;
      return (await this.updateFactory(factory)) !== undefined;
    } catch (error) {
      console.error("Error deleting factory:", error);
      throw new Error("Could not delete factory");
    }
  }

  /**
   * Logically deletes a chocolate.
   * @param {Chocolate} chocolate
   * @returns {Promise<boolean>}
   */
  static async deleteChocolate(chocolate) {
    try {
      chocolate.deleted = true;
      return (await this.updateChocolate(chocolate)) !== undefined;
    } catch (error) {
      console.error("Error deleting chocolate:", error);
      throw new Error("Could not delete chocolate");
    }
  }
}

export default ChocolateFactoryRepository;
