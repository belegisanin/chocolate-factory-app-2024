import path from "path";
import { readFile, writeFile } from "../utils/jsonFileUtils.js";
import Cart from "../models/Cart.js";

const cartsFilePath = path.join(process.cwd(), "/data/carts.json");

class CartRepository {
  static async generateCartId() {
    try {
      const entities = await this.getAllCarts();
      const lastEntity = entities[entities.length - 1];
      const lastId = lastEntity ? parseInt(lastEntity._id) : 0;
      return (lastId + 1).toString();
    } catch (error) {
      console.error("Error generating cart ID:", error);
      throw new Error("Could not generate cart ID");
    }
  }

  /**
   * Gets all carts from cart repository.
   *
   * @returns {Promise<Cart[]>}
   */
  static async getAllCarts() {
    try {
      const data = await readFile(cartsFilePath);
      //console.log(data);
      return data.map((cartJson) => Cart.fromJSON(cartJson));
    } catch (error) {
      console.error("Error getting all carts:", error);
      throw new Error("Could not retrieve carts");
    }
  }

  /**
   * Saves a cart to repository.
   *
   * @param {Cart} cart
   * @returns {Primise<Cart|undefined>}
   */
  static async saveCart(cart) {
    try {
      const carts = await this.getAllCarts();
      cart._id = await this.generateCartId();
      carts.push(cart);
      await writeFile(
        cartsFilePath,
        carts.map((c) => c.toJSON())
      );
      return cart;
    } catch (error) {
      console.error("Error saving cart:", error);
      throw new Error("Could not save cart");
    }
  }

  /**
   * Updates a cart in the repository.
   *
   * @param {Cart} cart
   * @returns {Promise<Cart|undefined>}
   */
  static async updateCart(cart) {
    try {
      const carts = await this.getAllCarts();
      const index = carts.findIndex((c) => c._id === cart._id);
      if (index === -1) return undefined;

      carts[index] = cart;

      await writeFile(
        cartsFilePath,
        carts.map((c) => c.toJSON())
      );

      return cart;
    } catch (error) {
      console.error("Error updating cart:", error);
      throw new Error("Could not update cart");
    }
  }
}

export default CartRepository;
