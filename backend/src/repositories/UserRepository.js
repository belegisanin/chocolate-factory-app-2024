import path from "path";
import { readFile, writeFile } from "../utils/jsonFileUtils.js";

import User from "../models/User.js";
import Purchase from "../models/Purchase.js";

const usersFilePath = path.join(process.cwd(), "/data/users.json");

class UserRepository {
  /**
   * Generates a new unique identifier for a user.
   * @returns {Promise<string>} A promise that resolves to a string representing the generated user ID.
   */
  static async generateId() {
    const users = await UserRepository.getAllUsers();
    const lastUser = users[users.length - 1];
    const lastId = lastUser ? parseInt(lastUser._id) : 0;
    return (lastId + 1).toString();
  }

  /**
   * Retrieves all users from the data file.
   * @returns {Promise<User[]>} A promise that resolves to an array of User objects.
   */
  static async getAllUsers() {
    const data = await readFile(usersFilePath);
    return data.map((customerTypeJSON) => User.fromJSON(customerTypeJSON));
  }

  /**
   * Finds a user by their username.
   *
   * @param {string} username - The username of the user to find.
   * @returns {Promise<User|undefined>} A promise that resolves to the User object with the specified username, or undefined if no user is found.
   */
  static async getByUsername(username) {
    const users = await UserRepository.getAllUsers();
    const found = users.find((user) => user.username === username);
    return found;
  }

  /**
   * Finds a user by their Id if exists, otherwise undefined.
   *
   * @param {string} userId - Id of a user.
   * @returns {Promise<User|undefined>}
   */
  static async getById(userId) {
    const users = await UserRepository.getAllUsers();
    const found = users.find((user) => user._id === userId);
    return found;
  }

  /**
   * Saves a new user to the data file.
   *
   * @param {User} user - The user object to save.
   * @returns {Promise<User>} A promise that resolves to the saved User object with the assigned ID.
   */
  static async saveUser(user) {
    const users = await UserRepository.getAllUsers();
    user._id = await UserRepository.generateId();
    users.push(user);
    await writeFile(usersFilePath, users);

    return user;
  }

  /**
   * Saves a cart.
   *
   * @param {Cart} cart
   * @returns {Promise<Cart|undefined>}
   */
  static async saveCart(cart) {
    const ownerId = cart.owner;
    const owner = await this.getById(ownerId);
    owner.cart = cart;
    const updated = await this.updateUser(owner);
    return updated.cart;
  }

  /**
   *
   * @param {User} user
   * @returns {Promise<User|undefined>}
   */
  static async updateUser(user) {
    try {
      const users = await UserRepository.getAllUsers();
      const index = users.findIndex((u) => u._id === user._id);

      if (index === -1) return undefined;

      users[index] = user;
      await writeFile(usersFilePath, users);
      return user;
    } catch (error) {
      console.error("Error updating user:", error);
      throw new Error("Could not update user");
    }
  }

  /**
   *
   * @param {Purchase} purchase
   * @returns {Promise<Purchase|undefined>}
   */
  static async updatePurchase(purchase) {
    try {
      let owner = await this.getById(purchase._owner);
      const index = owner.purchases.findIndex(
        (p) => p._purchaseId === purchase._purchaseId
      );

      owner.purchases[index] = purchase;
      const updatedOwner = await this.updateUser(owner);
      return purchase;
    } catch (error) {
      console.error("Error updating purchase:", error);
      throw new Error("Could not update purchase");
    }
  }
}

export default UserRepository;
