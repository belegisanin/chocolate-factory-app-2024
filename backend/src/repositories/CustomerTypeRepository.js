import path from "path";
import { readFile, writeFile } from "../utils/jsonFileUtils.js";

import CustomerType from "../models/CustomerType.js";

const customerTypesFilePath = path.join(
  process.cwd(),
  "/data/customer_types.json"
);

class CustomerTypeRepository {
  /**
   * Retrieve all customer types from the JSON file.
   *
   * @returns {Promise<[CustomerType]>} A Promise that resolves with the array of customer types.
   */
  static async getAllCustomerTypes() {
    const data = await readFile(customerTypesFilePath);
    return data.map((customerTypeJSON) =>
      CustomerType.fromJSON(customerTypeJSON)
    );
  }
}

export default CustomerTypeRepository;
