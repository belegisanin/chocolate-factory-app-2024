export const unauthorizedResponse = () => {
  return {
    message: "Unauthorized, You are not authorized perform this action.",
  };
};

export const badRequestResponse = (validator = undefined) => {
  return {
    message: "Bad Request.",
    errors: validator ? validator.errors : null,
  };
};

export const notFoundResponse = (id = undefined) => {
  return {
    message: id ? `Entity with _id=${id} not found.` : "Entity not found.",
  };
};
