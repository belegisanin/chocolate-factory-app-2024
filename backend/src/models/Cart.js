class Cart {
  constructor(
    owner,
    factory,
    factoryName,
    cartItems = [],
    totalPrice = 0,
    deleted = false
  ) {
    this.owner = owner;
    this.factory = factory;
    this.factoryName = factoryName;
    this.cartItems = cartItems;
    this.totalPrice = totalPrice;
    this.deleted = deleted;
  }

  static fromJSON(json) {
    if (!json) {
      throw new TypeError("Invalid JSON object: cannot be null or undefined");
    }
    return new Cart(
      json.owner,
      json.factory,
      json.factoryName,
      json.cartItems,
      json.totalPrice,
      json.deleted || false
    );
  }

  toJSON() {
    return {
      owner: this.owner,
      factory: this.factory,
      factoryName: this.factoryName,
      cartItems: this.cartItems || [],
      totalPrice: this.totalPrice || 0,
      deleted: this.deleted || false,
    };
  }
}

export default Cart;
