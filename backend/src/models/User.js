class User {
  constructor(
    _id,
    username,
    password,
    firstName,
    lastName,
    gender,
    birthDate,
    role,
    purchases,
    cart,
    factory,
    points,
    customerType,
    deleted = false
  ) {
    this._id = _id;
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.birthDate = birthDate;
    this.role = role;
    this.purchases = purchases;
    this.cart = cart;
    this.factory = factory;
    this.points = points;
    this.customerType = customerType;
    this.deleted = deleted;
  }

  static fromJSON(json) {
    return new User(
      json._id,
      json.username,
      json.password,
      json.firstName,
      json.lastName,
      json.gender,
      json.birthDate,
      json.role,
      json.purchases,
      json.cart,
      json.factory,
      json.points,
      json.customerType,
      json.deleted
    );
  }

  toJSON() {
    return {
      _id: this._id,
      username: this.username,
      password: this.password,
      firstName: this.firstName,
      lastName: this.lastName,
      gender: this.gender,
      birthDate: this.birthDate,
      role: this.role,
      purchases: this.purchases || [],
      cart: this.cart || null,
      factory: this.factory || null,
      points: this.points || 0,
      customerType: this.customerType,
      deleted: this.deleted || false,
    };
  }
}

export default User;
