import Cart from "./Cart.js";
import User from "./User.js";

class Purchase {
  constructor(
    _purchaseId,
    _owner,
    userFullName,
    chocolates = [],
    factory = null,
    factoryName = null,
    timestamp = Date.now(),
    totalPrice = 0,
    status = "Obrada"
  ) {
    this._purchaseId = _purchaseId;
    this._owner = _owner;
    this.userFullName = userFullName;
    this.chocolates = chocolates;
    this.factory = factory;
    this.factoryName = factoryName;
    this.timestamp = timestamp;
    this.totalPrice = totalPrice;
    this.status = status;
  }

  static fromJSON(json) {
    return new Purchase(
      json._purchaseId,
      json._owner,
      json.userFullName,
      json.chocolates,
      json.factory,
      json.factoryName,
      json.timestamp,
      json.totalPrice,
      json.status
    );
  }

  /**
   * Creates a purchase from a cart
   *
   * @param {User} user
   * @returns {Purchase}
   */
  static fromUser(user) {
    const cart = user.cart;

    return new Purchase(
      null,
      user._id,
      `${user.firstName} ${user.lastName}`,
      cart.cartItems,
      cart.factory,
      cart.factoryName,
      Date.now(),
      cart.totalPrice
    );
  }

  toJSON() {
    return {
      _purchaseId: this._purchaseId,
      _owner: this._owner,
      userFullName: this.userFullName,
      chocolates: this.chocolates,
      factory: this.factory || null,
      factoryName: this.factoryName,
      timestamp: this.timestamp,
      totalPrice: this.totalPrice || 0,
      status: this.status,
      deleted: this.deleted || false,
    };
  }
}

export default Purchase;
