class Location {
  constructor(longitude, latitude, adress) {
    this.longitude = longitude;
    this.latitude = latitude;
    this.adress = adress;
  }
}

export default Location;
