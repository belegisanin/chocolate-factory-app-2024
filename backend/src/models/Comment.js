class Comment {
  constructor(_id, user, factory, text, grade, status) {
    this._id = _id;
    this.user = user;
    this.factory = factory;
    this.text = text;
    this.grade = grade;
    this.status = status;
  }

  static fromJSON(json) {
    if (!json) {
      throw new TypeError("Invalid JSON object: cannot be null or undefined");
    }
    return new Comment(
      json._id,
      json.user,
      json.factory,
      json.text,
      json.grade,
      json.status
    );
  }

  toJSON() {
    return {
      _id: this._id,
      user: this.user,
      factory: this.factory,
      text: this.text,
      grade: this.grade,
      status: this.status || "Obrada",
      status: this.status,
    };
  }
}

export default Comment;
