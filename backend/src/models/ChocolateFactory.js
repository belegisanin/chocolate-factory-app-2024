class ChocolateFactory {
  /**
   *
   * @param {string} _id
   * @param {string} name
   * @param {string} workingHours
   * @param {string} logo
   * @param {Chocolate[]} chocolates
   * @param {string} status
   * @param {Location} location
   * @param {number} rating
   * @param {boolean} [deleted=false]
   */
  constructor(
    _id,
    name,
    workingHours,
    logo,
    comments,
    chocolates,
    status,
    location,
    rating,
    deleted = false
  ) {
    this._id = _id;
    this.name = name;
    this.workingHours = workingHours;
    this.logo = logo;
    this.comments = comments;
    this.chocolates = chocolates;
    this.status = status;
    this.location = location;
    this.rating = rating;
    this.deleted = deleted;
  }

  /**
   * Creates a ChocolateFactory instance from a JSON object.
   * @param {Object} json - The JSON object to convert.
   * @returns {ChocolateFactory} - The ChocolateFactory instance.
   */
  static fromJSON(json) {
    if (!json) {
      throw new TypeError("Invalid JSON object: cannot be null or undefined");
    }
    return new ChocolateFactory(
      json._id,
      json.name,
      json.workingHours,
      json.logo,
      json.comments,
      json.chocolates,
      json.status,
      json.location,
      json.rating,
      json.deleted
    );
  }

  /**
   * Converts the ChocolateFactory instance to a JSON object.
   * @returns {Object} - The JSON representation of the instance.
   */
  toJSON() {
    return {
      _id: this._id,
      name: this.name,
      workingHours: this.workingHours,
      logo: this.logo,
      comments: this.comments || [],
      chocolates: this.chocolates || [],
      status: this.status,
      location: this.location,
      rating: this.rating || 0,
      deleted: this.deleted || false,
    };
  }

  toFilteredJSON() {
    return {
      _id: this._id,
      name: this.name,
      workingHours: this.workingHours,
      logo: this.logo,
      comments: this.comments || [],
      chocolates: this.chocolates.filter((c) => !c.deleted),
      status: this.status,
      location: this.location,
      rating: this.rating || 0,
      deleted: this.deleted || false,
    };
  }
}

export default ChocolateFactory;
