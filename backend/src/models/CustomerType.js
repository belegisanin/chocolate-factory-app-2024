class CustomerType {
  constructor(name, discountAmount, requiredpoints) {
    this.name = name;
    this.discountAmount = discountAmount;
    this.requiredpoints = requiredpoints;
  }

  static fromJSON(json) {
    return new CustomerType(
      json.name,
      json.discountAmount,
      json.requiredpoints
    );
  }

  toJSON() {
    return {
      name: this.name,
      discountAmount: this.discountAmount,
      requiredpoints: this.requiredpoints,
    };
  }

  static fromPoints(points) {
    if (points >= 3000)
      return {
        name: "Zlatni",
        discountAmount: 20,
        requiredpoints: 3000,
      };
    else if (points >= 2000)
      return {
        name: "Srebrni",
        discountAmount: 10,
        requiredpoints: 2000,
      };
    else if (points >= 1000)
      return {
        name: "Bronzani",
        discountAmount: 5,
        requiredpoints: 1000,
      };
    else return null;
  }
}

export default CustomerType;
