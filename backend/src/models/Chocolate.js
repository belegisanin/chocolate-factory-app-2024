class Chocolate {
  /**
   * @param {string} _id
   * @param {string} _factoryId
   * @param {string} name
   * @param {number} price
   * @param {number} weight
   * @param {string} description
   * @param {string} image
   * @param {string} sort
   * @param {string} type
   * @param {string} status
   * @param {number} availableAmount
   * @param {boolean} [deleted=false]
   */
  constructor(
    _id,
    _factoryId,
    name,
    price,
    weight,
    description,
    image,
    sort,
    type,
    status,
    availableAmount,
    deleted = false
  ) {
    this._id = _id;
    this._factoryId = _factoryId;
    this.name = name;
    this.price = price;
    this.weight = weight;
    this.description = description;
    this.image = image;
    this.sort = sort;
    this.type = type;
    this.status = status;
    this.availableAmount = availableAmount;
    this.deleted = deleted;
  }

  /**
   * Creates a Chocolate instance from a JSON object.
   * @param {Object} json - JSON object representing a Chocolate.
   * @returns {Chocolate} A Chocolate instance.
   */
  static fromJSON(json) {
    if (!json) {
      throw new TypeError("Invalid JSON object: cannot be null or undefined");
    }
    return new Chocolate(
      json._id,
      json._factoryId,
      json.name,
      json.price,
      json.weight,
      json.description,
      json.image,
      json.sort,
      json.type,
      json.status,
      json.availableAmount,
      json.deleted || false
    );
  }

  /**
   * Creates a Chocolate instance from a request JSON object.
   * @param {Object} requestJson - JSON object from the request.
   * @param {string|null} [factoryId=null] - Factory ID.
   * @param {number} [availableAmount=0] - Available amount of chocolate.
   * @param {string} [status="Nema na stanju"] - Status of the chocolate.
   * @returns {Chocolate} A Chocolate instance.
   */
  static fromRequestJSON(
    requestJson,
    factoryId = null,
    availableAmount = 0,
    status = "Nema na stanju"
  ) {
    const c = this.fromJSON(requestJson);
    c._factoryId = factoryId !== null ? factoryId : c._factoryId;
    c.availableAmount = availableAmount;
    c.status = status;

    return c;
  }

  /**
   * Converts the Chocolate instance to a JSON object.
   * @returns {Object} JSON representation of the Chocolate instance.
   */
  toJSON() {
    return {
      _id: this._id,
      _factoryId: this._factoryId,
      name: this.name,
      price: this.price,
      weight: this.weight,
      description: this.description,
      image: this.image,
      sort: this.sort,
      type: this.type,
      status: this.status,
      availableAmount: this.availableAmount,
      deleted: this.deleted,
    };
  }
}

export default Chocolate;
