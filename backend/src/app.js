import express from "express";
import session from "express-session";
import cors from "cors";

import ChocolateFactoryService from "./services/ChocolateFactoryService.js";
import CustomerTypeService from "./services/CustomerTypeService.js";
import AuthService from "./services/AuthService.js";
import UserService from "./services/UserService.js";
import PurchaseService from "./services/PurchaseService.js";
import ImageService from "./services/ImageService.js";
import CommentService from "./services/CommentService.js";

const app = express();
app.use(
  cors({
    origin: "http://localhost:5173", // Replace with your frontend domain
    credentials: true,
  })
);
app.use(
  session({
    secret: "your-secret-key", // Replace with your own secret
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }, // Use secure: true in production with HTTPS
  })
);
app.use(express.json());
app.use("/static", express.static("public"));

// Routes for ChocolateFactories
app.get("/factories", ChocolateFactoryService.getAll);
app.get("/factories/:id", ChocolateFactoryService.getById);
app.get("/factories/:id/employees", UserService.getEmployeesByFactory);
app.post("/factories", ChocolateFactoryService.addFactory);
app.post("/factories/:id/chocolates", ChocolateFactoryService.addChocolate);
app.delete("/factories/:id", ChocolateFactoryService.deleteFactory);

// Routes for Chocolates
app.put("/chocolates", ChocolateFactoryService.updateChocolate);
app.delete("/chocolates/:chocolateId", ChocolateFactoryService.deleteChocolate);
app.put(
  "/chocolates/:chocolateId",
  ChocolateFactoryService.changeAmountOfChocolate
);

// Routes for Authentication
app.post("/auth/register", AuthService.registerCustomer);
app.post("/auth/login", AuthService.login);
app.get("/auth/getSession", AuthService.getSession);
app.get("/auth/destroySession", AuthService.destroySession);

// Route for CustomerTypes
app.get("/customerTypes", CustomerTypeService.getAll);

// Routes for Users
app.get("/user/:id", UserService.getUserById);
app.get("/users", UserService.getAllUsers);
app.put("/users", UserService.updateUser);
app.get("/users/managers/free", UserService.getFreeManagers);
app.post("/users/managers/assign", UserService.assignManagerToFactory);
app.post("/users/employees", UserService.addEmployeeToFactory);
app.post("/users/managers/check", UserService.isUsermanagerForFactory);

// Routes for purchasing
app.put("/cart/:ownerId", PurchaseService.updateCart);
app.post("/cart/add/:ownerId", PurchaseService.addToCart);
app.post("/cart/remove/:ownerId", PurchaseService.removeFromCart);
app.post("/cart/purchase/:ownerId", PurchaseService.makePurchase);
app.post("/carts/initialize", PurchaseService.initializeCart);
app.put("/purchase/cancel/:purchaseId", PurchaseService.cancelPurchase);
app.put("/purchase/approve/:purchaseId", PurchaseService.approvePurchase);
app.put("/purchase/reject/:purchaseId", PurchaseService.rejectPurchase);
app.get("/purchases/:factoryId", PurchaseService.getPurchasesForFactory);

// Routes for comments
app.put("/comments/approve/:commentId", CommentService.approveComment);
app.put("/comments/reject/:commentId", CommentService.rejectComment);
app.post("/comments/:factoryId", CommentService.makeComment);
app.get(
  "/comments/isAllowed/:userId/:factoryId",
  CommentService.isAllowedToComment
);

// Routes for images/resources
app.post(
  "/images",
  ImageService.upload.single("image"),
  ImageService.uploadImage
);

// Handle 404 errors
app.use((req, res) => {
  res.status(404).send("Not Found");
});

export default app;
