import Ajv from "ajv";
import addFormats from "ajv-formats";

import UserController from "../controllers/UserController.js";

import {
  unauthorizedResponse,
  badRequestResponse,
  notFoundResponse,
} from "../utils/errorUtils.js";

import {
  assignManagerRequestSchema,
  addEmployeeRequestSchema,
  checkManagerRequestSchema,
  updateUserRequestSchema,
} from "../schemas/requestSchemas.js";

const ajv = new Ajv({ allErrors: true, $data: true });
addFormats(ajv);

const validateAssignManager = ajv.compile(assignManagerRequestSchema);
const validateAddEmployee = ajv.compile(addEmployeeRequestSchema);
const validateCheckManager = ajv.compile(checkManagerRequestSchema);
const validateUpdateUser = ajv.compile(updateUserRequestSchema);

class UserService {
  static userController = new UserController();

  static async getAllUsers(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const session = req.session.user;
      if (!session || session.role !== "administrator")
        return res.status(401).json(unauthorizedResponse());

      const users = await UserService.userController.getAll();
      res.status(200).json(users);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async getUserById(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const userId = req.params.id;
      const user = await UserService.userController.getById(userId);

      if (!user) return res.status(404).json(notFoundResponse(userId));

      return res.status(200).json(user);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async updateUser(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const valid = validateUpdateUser(req.body);
      if (!valid)
        return res.status(400).json(badRequestResponse(validateUpdateUser));

      const userId = req.body._id;
      let user = await UserService.userController.getById(userId);
      if (!user) return res.status(404).json(notFoundResponse(userId));

      const session = req.session.user;
      //console.log(session);
      if (!session || session._id !== user._id)
        return res.status(401).json(unauthorizedResponse());

      user = { ...user, ...req.body };
      const updatedUser = await UserService.userController.updateUser(user);

      if (!updatedUser)
        res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "User successfuly updated.", user: updatedUser });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   */
  static async getFreeManagers(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      let managers = await UserService.userController.getFreeManagers();
      res.status(200).json(managers);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async assignManagerToFactory(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const valid = validateAssignManager(req.body);
      const role = req.session.user ? req.session.user.role : undefined;

      if (role !== "administrator")
        return res.status(401).json(unauthorizedResponse());

      if (!valid)
        return res.status(400).json(badRequestResponse(validateAssignManager));

      const managerId = req.body.managerId;
      const factoryId = req.body.factoryId;
      const manager = await UserService.userController.asignManagerToFactory(
        managerId,
        factoryId
      );

      if (manager) {
        return res
          .status(200)
          .json({ message: "Manager successfuly assigned", manager: manager });
      } else return res.status(500).json({ error: "Internal Server Error" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log = true]
   * @returns
   */
  static async addEmployeeToFactory(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const valid = validateAddEmployee(req.body);
      const role = req.session.user ? req.session.user.role : undefined;
      const factoryId = req.body.factory;
      const isManager = req.session.user
        ? UserService.userController.isManagerForFactory(
            req.session.user._id,
            factoryId
          )
        : undefined;

      if (!isManager) return res.status(401).json(unauthorizedResponse());

      if (!valid)
        return res.status(400).json(badRequestResponse(validateAddEmployee));

      const employee =
        await UserService.userController.registerEmployeeToFactory(
          req.body,
          factoryId
        );

      if (employee) {
        return res.status(201).json({
          message: "Employee successfuly added to factory.",
          employee: employee,
        });
      } else return res.status(500).json({ error: "Internal Server Error" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   * @returns
   */
  static async getEmployeesByFactory(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const factoryId = req.params.id;
      const employees =
        await UserService.userController.getAllEmployeesByFactory(factoryId);

      return res.status(200).json(employees);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async isUsermanagerForFactory(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const valid = validateCheckManager(req.body);

      if (!valid)
        return res.status(400).json(badRequestResponse(validateCheckManager));

      const userId = req.body.userId;
      const factoryId = req.body.factoryId;

      const isManager = await UserService.userController.isManagerForFactory(
        userId,
        factoryId
      );

      if (isManager)
        return res.status(200).json({ isManager, userId, factoryId });
      else return res.status(200).json({ isManager });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default UserService;
