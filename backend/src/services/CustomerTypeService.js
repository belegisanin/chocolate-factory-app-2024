import CustomerTypeController from "../controllers/CustomerTypeController.js";

class CustomerTypeService {
  static controller = new CustomerTypeController();

  /**
   * Retrieve all customer types.
   *
   * This method fetches all customer types from the database
   * using the CustomerTypeController and returns them as JSON.
   *
   * @param {Request} req - The request object.
   * @param {Response} res - The response object.
   * @param {boolean} [log=true] - Whether to log the request endpoint.
   * @returns {Promise<void>} A Promise that resolves when the operation is complete.
   */
  static async getAll(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.path}'`);

    try {
      const data = await CustomerTypeService.controller.getAll();
      res.status(200).json(data);
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default CustomerTypeService;
