import CommentController from "../controllers/CommentController.js";

import Ajv from "ajv";
import addFormats from "ajv-formats";

import { makeCommentRequestSchema } from "../schemas/requestSchemas.js";

import {
  unauthorizedResponse,
  badRequestResponse,
  notFoundResponse,
} from "../utils/errorUtils.js";

const ajv = new Ajv({ allErrors: true, $data: true });
addFormats(ajv);

const validateMakeComment = ajv.compile(makeCommentRequestSchema);

class CommentService {
  static commentController = new CommentController();

  static async approveComment(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const commentId = req.params.commentId;

      const approved = await CommentService.commentController.approveComment(
        commentId
      );
      if (!approved)
        return res.status(500).json({ error: "Internal Server Error" });
      else return res.status(200).json({ comment: approved });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async rejectComment(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const commentId = req.params.commentId;

      const rejected = await CommentService.commentController.rejectComment(
        commentId
      );
      if (!rejected)
        return res.status(500).json({ error: "Internal Server Error" });
      else return res.status(200).json({ comment: rejected });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async isAllowedToComment(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const userId = req.params.userId;
      const factoryId = req.params.factoryId;

      const isAllowed =
        await CommentService.commentController.isAllowedToPostComment(
          userId,
          factoryId
        );

      return res.status(200).json({ isAllowed: isAllowed });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async makeComment(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const valid = validateMakeComment(req.body);
      if (!valid)
        return res.status(400).json(badRequestResponse(validateMakeComment));

      const factoryId = req.params.factoryId;
      const username = req.body.username;
      const text = req.body.text;
      const grade = req.body.grade;

      const comment = await CommentService.commentController.makeComment(
        username,
        text,
        grade,
        factoryId
      );

      if (!comment)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Comment successfuly added.", comment: comment });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default CommentService;
