import ChocolateFactoryController from "../controllers/ChocolateFactoryController.js";
import PurchaseController from "../controllers/PurchaseController.js";
import Cart from "../models/Cart.js";

import {
  unauthorizedResponse,
  badRequestResponse,
  notFoundResponse,
} from "../utils/errorUtils.js";

class PurchaseService {
  static purchaseController = new PurchaseController();
  static chocolateFactoryController = new ChocolateFactoryController();

  static async getAllCarts(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const carts = await PurchaseService.purchaseController.getAllCarts();
      res.status(200).json(carts);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async getPurchasesForFactory(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const factoryId = req.params.factoryId;
      const purchases =
        await PurchaseService.purchaseController.getAllPurchasesForFactory(
          factoryId
        );

      return res.status(200).json(purchases);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async getCartByOwner(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    try {
      const ownerId = req.params.id;

      if (!ownerId) return res.status(400).json(badRequestResponse());

      const cart = await PurchaseService.purchaseController.getCartByUser(
        ownerId
      );
      if (!cart)
        return res
          .status(404)
          .json({ message: "Cart not found for this user." });
      else return res.status(200).json({ cart: cart });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async initializeCart(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const session = req.session.user;
      if (!session) return res.status(401).json(unauthorizedResponse());

      const cart = await PurchaseService.purchaseController.getCartByUser(
        session._id
      );
      if (!cart)
        return res
          .status(400)
          .json({ message: "Cart for this user already exists." });

      const newCart = await PurchaseService.purchaseController.initializeCart(
        session._id
      );
      return res
        .status(201)
        .json({ message: "Cart successfuly initialized.", cart: newCart });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async updateCart(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const userId = req.params.ownerId;
      const requestCart = Cart.fromJSON(req.body);
      const cart = await PurchaseService.purchaseController.getCartByUser(
        userId
      );

      if (!cart) return res.status(404).json(notFoundResponse());

      const updatedCart = await PurchaseService.purchaseController.updateCart(
        requestCart
      );

      if (!updatedCart)
        res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Cart successfuly updated.", cart: updatedCart });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async addToCart(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const ownerId = req.params.ownerId;
      let cart = await PurchaseService.purchaseController.getCartByUser(
        ownerId
      );

      if (!cart || cart.length == 0) {
        cart = await PurchaseService.purchaseController.initializeCart(ownerId);
        console.log("init");
        console.log(cart);
      }

      const chocolateId = req.body.chocolateId;
      const amount = req.body.amount;

      const chocolate =
        await PurchaseService.chocolateFactoryController.getChocolateById(
          chocolateId
        );
      const updatedCart = await PurchaseService.purchaseController.addToCart(
        cart,
        chocolate,
        amount
      );

      if (!updatedCart)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Item added to cart.", cart: updatedCart });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async removeFromCart(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);
    try {
      const ownerId = req.params.ownerId;
      const chocolateId = req.body.chocolateId;
      const cart = await PurchaseService.purchaseController.getCartByUser(
        ownerId
      );
      const chocolate =
        await PurchaseService.chocolateFactoryController.getChocolateById(
          chocolateId
        );

      const updatedCart =
        await PurchaseService.purchaseController.removeFromCart(
          cart,
          chocolate
        );

      if (!updatedCart)
        return res.status(500).json({ error: "Internal Server Error" });
      else return res.status(200).json({ cart: updatedCart });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async makePurchase(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      const userId = req.params.ownerId;
      const cart = await PurchaseService.purchaseController.getCartByUser(
        userId
      );

      if (!cart) return res.status(404).json(notFoundResponse());

      const purchase = await PurchaseService.purchaseController.makePurchase(
        cart
      );
      if (!purchase)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Purchase successful.", purchase: purchase });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   * Approves a purchase.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} log
   * @returns
   */
  static async approvePurchase(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const purchaseId = req.params.purchaseId;
      const approved = await PurchaseService.purchaseController.approvePurchase(
        purchaseId
      );

      if (!approved)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Purchase approved.", purchase: approved });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   * Rejects a purchase.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} log
   * @returns
   */
  static async rejectPurchase(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const purchaseId = req.params.purchaseId;
      const rejectMessage = req.body.rejectMessage;
      const rejected = await PurchaseService.purchaseController.rejectPurchase(
        purchaseId,
        rejectMessage
      );

      if (!rejected)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res
          .status(200)
          .json({ message: "Purchase rejected.", purchase: rejected });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   * Cancels a purchase.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   */
  static async cancelPurchase(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.url}'`);

    try {
      const purchaseId = req.params.purchaseId;
      const canceledPurchase =
        await PurchaseService.purchaseController.cancelPurchase(purchaseId);
      if (!canceledPurchase)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res.status(200).json({
          message: "Purchase successfuly canceled.",
          purchase: canceledPurchase,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default PurchaseService;
