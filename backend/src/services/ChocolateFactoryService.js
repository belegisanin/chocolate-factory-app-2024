import express from "express";
import Ajv from "ajv";
import addFormats from "ajv-formats";

import Chocolate from "../models/Chocolate.js";
import ChocolateFactoryController from "../controllers/ChocolateFactoryController.js";
import ChocolateFactory from "../models/ChocolateFactory.js";

import {
  unauthorizedResponse,
  badRequestResponse,
} from "../utils/errorUtils.js";

import {
  addChocolateRequestSchema,
  addChocolateFactoryRequestSchema,
  updateChocolateRequestSchema,
} from "../schemas/requestSchemas.js";
import UserController from "../controllers/UserController.js";

const ajv = new Ajv({ allErrors: true, $data: true });
addFormats(ajv);

const validateAddChocolate = ajv.compile(addChocolateRequestSchema);
const validateAddFactory = ajv.compile(addChocolateFactoryRequestSchema);
const validateUpdateChocolate = ajv.compile(updateChocolateRequestSchema);

class ChocolateFactoryService {
  static controller = new ChocolateFactoryController();
  static userController = new UserController();

  static async getAll(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.path}'`);

    try {
      const data = (
        await ChocolateFactoryService.controller.getAllFactories()
      ).map((f) => f.toFilteredJSON());
      res.status(200).json(data);
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async getById(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.path}'`);

    try {
      const factoryId = req.params.id;
      const data = await ChocolateFactoryService.controller.getFactoryById(
        factoryId
      );
      res.status(200).json(data.toFilteredJSON());
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   * Adds a new Chocolate Factory.
   *
   * This method handles POST requests to '/factories' endpoint.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   * @returns
   */
  static async addFactory(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.path}'`);

    try {
      const valid = validateAddFactory(req.body);
      const role = req.session.user ? req.session.user.role : undefined;

      //console.log(session);

      if (role !== "administrator")
        return res.status(401).json(unauthorizedResponse());

      if (!valid)
        return res.status(400).json(badRequestResponse(validateAddFactory));

      const factory = await ChocolateFactoryService.controller.addFactory(
        ChocolateFactory.fromJSON(req.body)
      );

      if (factory) {
        res.status(201).json({
          message: "ChocolateFactory successfuly added.",
          factory: factory,
        });
      } else {
        res.status(500).json({ error: "Internal Server Error" });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  /**
   * Method to handle adding chocolates to factories.
   *
   * @param {express.Request} req - Request object.
   * @param {express.Response} res - Response object.
   * @param {boolean} log - Set to true if hits to the endpoint should be logged to console.
   */
  static async addChocolate(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.path}'`);

    try {
      const factory = await ChocolateFactoryService.controller.getFactoryById(
        req.params.id
      );
      if (factory === undefined)
        return res.status(400).json({
          message: "Bad Request",
          errors: `Chocolate factory with _id '${req.params.id}' does not exist.`,
        });

      const valid = validateAddChocolate(req.body);
      if (!valid)
        return res.status(400).json({
          message: "Bad Request",
          errors: validateAddChocolate.errors,
        });

      const chocolate = Chocolate.fromRequestJSON(req.body, factory._id);
      const result = await ChocolateFactoryService.controller.addChocolate(
        chocolate,
        factory._id
      );

      if (result != undefined)
        return res.status(201).json({
          message: "Chocolate successfuly added.",
          data: result,
        });
      else return res.status(500).json({ error: "Internal Server Error" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async updateChocolate(req, res, log = true) {
    if (log) console.log(`PUT on endpoint '${req.path}'`);

    try {
      const valid = validateUpdateChocolate(req.body);

      if (!valid)
        return res.status(400).json({
          message: "Bad Request",
          errors: validateUpdateChocolate.errors,
        });

      const chocolate = Chocolate.fromJSON(req.body);
      const result = await ChocolateFactoryService.controller.updateChocolate(
        chocolate
      );

      if (result != undefined)
        return res
          .status(201)
          .json({ message: "Chocolate successfuly updated.", data: result });
      else return res.status(500).json({ error: "Internal Server Error" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async deleteFactory(req, res, log = true) {
    if (log) console.log(`DELETE on endpoint '${req.path}'`);

    try {
      const factoryId = req.params.id;
      const factory = await ChocolateFactoryService.controller.getFactoryById(
        factoryId
      );

      if (!factory)
        return res.status(404).json({
          message: `Chocolate factory with _id=${factoryId} not found.`,
        });

      const result = await ChocolateFactoryService.controller.deleteFactory(
        factory
      );

      if (result)
        return res.status(200).json({
          message: "ChocolateFactory successfuly deleted.",
          success: true,
        });
      else
        res
          .status(500)
          .json({ message: "Internal Server Error", success: false });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async deleteChocolate(req, res, log = true) {
    if (log) console.log(`DELETE request on endpoint '${req.path}'`);

    try {
      const chocolateId = req.params.chocolateId;
      const chocolate =
        await ChocolateFactoryService.controller.getChocolateById(chocolateId);

      if (!chocolate)
        return res.status(404).json({
          message: `Chocolate with _id=${chocolateId} not found.`,
        });

      const result = await ChocolateFactoryService.controller.deleteChocolate(
        chocolate
      );

      if (result)
        return res.status(200).json({
          message: "Chocolate successfuly deleted.",
          success: true,
        });
      else
        res
          .status(500)
          .json({ message: "Internal Server Error", success: false });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  static async changeAmountOfChocolate(req, res, log = true) {
    if (log) console.log(`PUT request on endpoint '${req.path}'`);

    try {
      const chocolateId = req.params.chocolateId;
      const newAmount = req.body.newAmount;

      if (!req.session.user)
        return res.status(401).json(unauthorizedResponse());

      const chocolate =
        await ChocolateFactoryService.controller.getChocolateById(chocolateId);

      if (!chocolate || newAmount < 0)
        return res.status(400).json(badRequestResponse());

      const isEmployee =
        await ChocolateFactoryService.userController.isEmployeeForFactory(
          req.session.user._id,
          chocolate._factoryId
        );

      if (!isEmployee) return res.status(401).json(unauthorizedResponse());

      const newChocolate =
        await ChocolateFactoryService.controller.changeAmountOfChocolate(
          chocolateId,
          newAmount
        );

      if (!newChocolate)
        return res.status(500).json({ error: "Internal Server Error" });
      else
        return res.status(200).json({
          message: "Amount successfuly updated.",
          chocolate: newChocolate,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default ChocolateFactoryService;
