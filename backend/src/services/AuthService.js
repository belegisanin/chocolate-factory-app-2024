import Ajv from "ajv";
import addFormats from "ajv-formats";
import UserController from "../controllers/UserController.js";
import {
  registrationRequestSchema,
  loginRequestSchema,
} from "../schemas/requestSchemas.js";

const ajv = new Ajv({ allErrors: true, $data: true });
addFormats(ajv);

const validateRegister = ajv.compile(registrationRequestSchema);
const validateLogin = ajv.compile(loginRequestSchema);

class AuthService {
  static userController = new UserController();

  /**
   * Register a new User with default type Customer(Kupac)
   *
   * This method handles POST requests to '/auth/register' endpoint.
   *
   * @param {Request} req - The request object.
   * @param {Response} res - The response object.
   * @param {boolean} [log=true] - Whether to log the request endpoint.
   * @returns {Promise<void>} A Promise that resolves when the operation is complete.
   */
  static async registerCustomer(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    const data = req.body;
    const valid = validateRegister(data);

    if (!valid) {
      return res.status(400).json({
        message: "Bad Request",
        errors: validateRegister.errors,
      });
    }

    if (
      (await AuthService.userController.getByUsername(data.username)) != null
    ) {
      return res.status(400).json({
        message: "Username already exists.",
      });
    }

    try {
      const user = await AuthService.userController.registerCustomer(data);
      res.status(201).json({
        message: "User successfully registered.",
        user: { _id: user._id },
      });
    } catch (error) {
      console.error("An error occurred:", error);
      res.status(500).json({ message: "Internal Server Error" });
    }
  }

  /**
   * Used for login of all types of users.
   *
   * This method handles POST requests to '/auth/login' endpoint.
   *
   * @param {Request} req - The request object.
   * @param {Response} res - The response object.
   * @param {boolean} [log=true] - Whether to log the request endpoint.
   * @returns {Promise<void>} A Promise that resolves when the operation is complete.
   */
  static async login(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    const data = req.body;
    const valid = validateLogin(data);

    if (!valid) {
      return res.status(400).json({
        message: "Bad Request",
        errors: validateLogin.errors,
      });
    }

    const user = await AuthService.userController.getByUsername(data.username);

    if (!user || user.password !== data.password) {
      return res.status(401).json({ message: "Invalid username or password." });
    }

    const user_info = {
      _id: user._id,
      username: user.username,
      role: user.role,
      factory: user.factory,
    };

    console.log(user_info);
    req.session.user = user_info;

    // Save the session before sending the response
    req.session.save((err) => {
      if (err) {
        console.error("Error saving session:", err);
        return res.status(500).json({ message: "Internal Server Error" });
      }

      res.status(200).json({
        message: "Login successful.",
        session: user_info,
      });
    });
  }

  /**
   * Gets the session data if it exists.
   *
   * This method handles GET requests to '/auth/getSession' endpoint.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   */
  static async getSession(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    if (req.session.user) {
      res.status(200).json({
        session: req.session.user,
      });
    } else {
      res.status(404).json({
        message: "No session data found.",
      });
    }
  }

  /**
   * Destroys the current session.
   *
   * This method handles GET requests to '/auth/destroySession' endpoint.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   */
  static async destroySession(req, res, log = true) {
    if (log) console.log(`GET on endpoint '${req.url}'`);

    req.session.destroy((err) => {
      if (err) {
        console.error("Error destroying session:", err);
        res.status(500).json({ message: "Error destroying session" });
      } else {
        res.status(200).json({ message: "Session destroyed" });
      }
    });
  }
}

export default AuthService;
