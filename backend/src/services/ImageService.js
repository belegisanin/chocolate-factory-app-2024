import multer from "multer";
import path from "path";

class ImageService {
  static upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, "public/images");
      },
      filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
        cb(
          null,
          file.fieldname + "-" + uniqueSuffix + path.extname(file.originalname)
        );
      },
    }),
  });

  /**
   * Saves an image to the static resources.
   *
   * This method handles POST requests to '/images' endpoint.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {boolean} [log=true]
   */
  static async uploadImage(req, res, log = true) {
    if (log) console.log(`POST on endpoint '${req.url}'`);

    try {
      if (!req.file) {
        return res.status(400).json({ message: "No file uploaded" });
      }

      const { filename, path: filePath, mimetype, size } = req.file;

      res.status(200).json({
        message: "Image uploaded successfully",
        file: {
          filename,
          path: filePath,
          mimetype,
          size,
        },
      });
    } catch (error) {
      console.error("Error uploading image:", error);
      res.status(500).json({ message: "Error uploading image" });
    }
  }
}

export default ImageService;
