export const registrationRequestSchema = {
  type: "object",
  properties: {
    username: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      pattern: "^[a-zA-Z0-9._-]+$",
    },
    password: {
      type: "string",
      minLength: 8,
    },
    password_repeated: {
      type: "string",
      const: { $data: "1/password" },
    },
    firstName: {
      type: "string",
      minLength: 1,
      maxLength: 50,
    },
    lastName: {
      type: "string",
      minLength: 1,
      maxLength: 50,
    },
    gender: {
      type: "string",
      enum: ["Muški", "Ženski"],
    },
    dateOfBirth: {
      type: "string",
      format: "date",
    },
  },
  required: [
    "username",
    "password",
    "password_repeated",
    "firstName",
    "lastName",
    "gender",
    "dateOfBirth",
  ],
  additionalProperties: false,
};

export const loginRequestSchema = {
  type: "object",
  properties: {
    username: {
      type: "string",
    },
    password: {
      type: "string",
    },
  },
  required: ["username", "password"],
  additionalProperties: false,
};

export const addChocolateRequestSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
    },
    price: {
      type: "number",
    },
    weight: {
      type: "number",
    },
    description: {
      type: "string",
    },
    image: {
      type: "string",
    },
    sort: {
      type: "string",
      enum: ["Obična", "Za kuvanje", "Za piće"],
    },
    type: {
      type: "string",
      enum: ["Crna", "Bela", "Mlečna"],
    },
  },
  required: ["name", "price", "weight", "description", "sort", "type", "image"],
  additionalProperties: false,
};

export const addChocolateFactoryRequestSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
    },
    location: {
      type: "object",
      properties: {
        longitude: {
          type: "number",
        },
        latitude: {
          type: "number",
        },
        adress: {
          type: "string",
        },
      },
      required: ["longitude", "latitude", "adress"],
    },
    managerId: {
      type: "string",
    },
    workingHours: {
      type: "string",
    },
    logo: {
      type: "string",
    },
  },
  required: ["name", "location", "managerId", "workingHours", "logo"],
  additionalProperties: false,
};

export const updateChocolateRequestSchema = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    _factoryId: {
      type: "string",
    },
    name: {
      type: "string",
    },
    price: {
      type: "number",
    },
    weight: {
      type: "number",
    },
    description: {
      type: "string",
    },
    image: {
      type: "string",
    },
    sort: {
      type: "string",
      enum: ["Obična", "Za kuvanje", "Za piće"],
    },
    type: {
      type: "string",
      enum: ["Crna", "Bela", "Mlečna"],
    },
    status: {
      type: "string",
      enum: ["Na stanju", "Nema na stanju"],
    },
    availableAmount: {
      type: "number",
    },
  },
  required: [
    "_id",
    "_factoryId",
    "name",
    "price",
    "weight",
    "description",
    "sort",
    "type",
    "image",
    "status",
    "availableAmount",
  ],
};

export const assignManagerRequestSchema = {
  type: "object",
  properties: {
    managerId: {
      type: "string",
    },
    factoryId: {
      type: "string",
    },
  },
  required: ["managerId", "factoryId"],
  additionalProperties: false,
};

export const addEmployeeRequestSchema = {
  type: "object",
  properties: {
    username: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      pattern: "^[a-zA-Z0-9._-]+$",
    },
    password: {
      type: "string",
      minLength: 8,
    },
    password_repeated: {
      type: "string",
      const: { $data: "1/password" },
    },
    firstName: {
      type: "string",
      minLength: 1,
      maxLength: 50,
    },
    lastName: {
      type: "string",
      minLength: 1,
      maxLength: 50,
    },
    gender: {
      type: "string",
      enum: ["Muški", "Ženski"],
    },
    dateOfBirth: {
      type: "string",
      format: "date",
    },
    factory: {
      type: "string",
    },
  },
  required: [
    "username",
    "password",
    "password_repeated",
    "firstName",
    "lastName",
    "gender",
    "dateOfBirth",
    "factory",
  ],
  additionalProperties: false,
};

export const checkManagerRequestSchema = {
  type: "object",
  properties: {
    userId: {
      type: "string",
    },
    factoryId: {
      type: "string",
    },
  },
  required: ["userId", "factoryId"],
  additionalProperties: false,
};

export const updateUserRequestSchema = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    firstName: {
      type: "string",
    },
    lastName: {
      type: "string",
    },
    username: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      pattern: "^[a-zA-Z0-9._-]+$",
    },
    gender: {
      type: "string",
      enum: ["Muški", "Ženski"],
    },
    birthDate: {
      type: "string",
      format: "date",
    },
  },
  required: ["_id", "firstName", "lastName", "username", "birthDate", "gender"],
};

export const makeCommentRequestSchema = {
  type: "object",
  properties: {
    username: {
      type: "string",
    },
    text: {
      type: "string",
    },
    grade: {
      type: "number",
    },
  },
  required: ["username", "text", "grade"],
  additionalProperties: false,
};
