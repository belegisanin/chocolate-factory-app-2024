import CustomerTypeRepository from "../repositories/CustomerTypeRepository.js";

class CustomerTypeController {
  /**
   * Retrieve all customer types.
   *
   * This method delegates the retrieval of all customer types
   * to the CustomerTypeRepository and returns the result.
   *
   * @returns {Promise<[CustomerType]>} A Promise that resolves with the array of customer types.
   */
  getAll() {
    return CustomerTypeRepository.getAllCustomerTypes();
  }
}

export default CustomerTypeController;
