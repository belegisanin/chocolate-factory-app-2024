import UserRepository from "../repositories/UserRepository.js";
import ChocolateFactoryController from "./ChocolateFactoryController.js";
import User from "../models/User.js";

class UserController {
  constructor() {
    this.chocolateFactoryController = new ChocolateFactoryController();
  }

  /**
   * Retrieves all users from the database.
   *
   * @returns {Promise<[User]>} A promise of the list of all users.
   */
  async getAll() {
    const users = await UserRepository.getAllUsers();
    return users.filter((u) => !u.deleted);
  }

  /**
   * Retrieves a user by username if it exists or undefined.
   *
   * @param {string} username Usename value.
   * @returns {Promise<User | undefined>} A promise of User object if exists or null.
   */
  getByUsername(username) {
    return UserRepository.getByUsername(username);
  }

  /**
   * Retrieves a user by username if it exists or null.
   *
   * @param {string} userId User Id.
   * @returns {Promise<User | undefined>} A promise of User object if exists or null.
   */
  getById(userId) {
    return UserRepository.getById(userId);
  }

  /**
   * Updates an user.
   *
   * @param {User} user
   * @returns {Promise<User|undefined>}
   */
  updateUser(user) {
    return UserRepository.updateUser(user);
  }

  /**
   * Retrieves all employees that work in a factory.
   *
   * @param {string} factoryId
   * @returns {Promise<User[]>}
   */
  async getAllEmployeesByFactory(factoryId) {
    const users = await this.getAll();
    const employees = users.filter(
      (u) => u.role === "employee" && u.factory === factoryId
    );
    return employees;
  }

  /**
   * Adds a new employee to the factory.
   *
   * @param {JSON} employeeJson
   * @param {string} factoryId
   * @returns {Promise<User|undefined>}
   */
  async registerEmployeeToFactory(employeeJson, factoryId) {
    let employee = User.fromJSON(employeeJson);
    let factory = await this.chocolateFactoryController.getFactoryById(
      factoryId
    );

    if (!factory) return undefined;

    employee.role = "employee";
    employee.factory = factory._id;
    employee.points = 0;

    return UserRepository.saveUser(employee);
  }

  /**
   * Sets up a User object as a Customer(Kupac) and saves it to the JSON database.
   *
   * @param {JSON} registerJson Usename value.
   * @returns {Promise<User | null>} A promise of User object if successful or null.
   */
  registerCustomer(registerJson) {
    let customer = User.fromJSON(registerJson);
    customer.role = "customer";
    customer.points = 0;

    return UserRepository.saveUser(customer);
  }

  /**
   * Gets all managers that are not asigned to a factory.
   *
   * @returns {Promise<User[]>}
   */
  async getFreeManagers() {
    let managers = (await this.getAll())
      .filter((u) => u.role === "manager" && u.factory == null)
      .map((m) => ({
        _id: m._id,
        username: m.username,
        firstName: m.firstName,
        lastName: m.lastName,
        fullName: `${m.firstName} ${m.lastName}`,
      }));
    return managers;
  }

  /**
   * Checks if user is a manager at given factory.
   *
   * @param {string} managerId
   * @param {string} factoryId
   * @returns {Promise<boolean>}
   */
  async isManagerForFactory(managerId, factoryId) {
    const manager = await this.getById(managerId);
    return (
      manager && manager.role === "manager" && manager.factory === factoryId
    );
  }

  /**
   * Checks if user is a employee at given factory.
   *
   * @param {string} employeeId
   * @param {string} factoryId
   * @returns {Promise<boolean>}
   */
  async isEmployeeForFactory(employeeId, factoryId) {
    const employee = await this.getById(employeeId);
    return (
      employee && employee.role === "employee" && employee.factory === factoryId
    );
  }

  /**
   * Assigns a manager to a factory.
   *
   * @param {string} managerId
   * @param {string} factoryId
   * @returns {Promise<User|undefined>}
   */
  async asignManagerToFactory(managerId, factoryId) {
    let manager = await this.getById(managerId);
    let factory = await this.chocolateFactoryController.getFactoryById(
      factoryId
    );

    if (!(manager && factory)) return undefined;

    manager.factory = factory._id;

    return UserRepository.updateUser(manager);
  }
}

export default UserController;
