import Cart from "../models/Cart.js";
import Chocolate from "../models/Chocolate.js";
import CustomerType from "../models/CustomerType.js";
import Purchase from "../models/Purchase.js";
import User from "../models/User.js";
//import CartRepository from "../repositories/CartRepository.js";
import UserRepository from "../repositories/UserRepository.js";
import ChocolateFactoryController from "./ChocolateFactoryController.js";
import UserController from "./UserController.js";

class PurchaseController {
  constructor() {
    this.userController = new UserController();
    this.factoryController = new ChocolateFactoryController();
  }

  /**
   * Initializes an empty cart for user with userId.
   *
   * @param {string} userId
   * @returns {Promise<Cart|undefined>}
   */
  async initializeCart(userId) {
    // const cart = new Cart(null, userId);
    // return CartRepository.saveCart(cart);
    const user = await UserRepository.getById(userId);
    user.cart = new Cart(userId, null, null);
    return UserRepository.saveCart(user.cart);
  }

  /**
   * Gets a cart by userId.
   *
   * @param {string} userId
   * @returns {Promise<Cart|undefined>}
   */
  async getCartByUser(userId) {
    const user = await UserRepository.getById(userId);
    return user.cart;
  }

  /**
   * Gets all purchases;
   *
   * @returns {Promise<Purchase[]>}
   */
  async getAllPurchases() {
    const users = await UserRepository.getAllUsers();
    let purchases = [];

    users.forEach((user) => {
      user.purchases.forEach((purchase) => {
        purchases.push(purchase);
      });
    });

    return purchases.filter((p) => !p.deleted);
  }

  /**
   * Gets all purchases for factory.
   *
   * @param {string} factoryId
   * @returns {Promise<Purchase[]>}
   */
  async getAllPurchasesForFactory(factoryId) {
    const allPurchases = await this.getAllPurchases();
    const factorypurchases = allPurchases.filter(
      (p) => p.factory === factoryId
    );
    return factorypurchases;
  }

  /**
   * Gets a purchase by _purchaseId.
   *
   * @param {string} purchaseId
   * @returns {Promise<Purchase|undefined>}
   */
  async getPurchaseById(purchaseId) {
    const purchases = await this.getAllPurchases();
    const found = purchases.find((p) => p._purchaseId == purchaseId);
    return found;
  }

  /**
   * Updates a cart.
   *
   * @param {Cart} cart
   * @returns {Promise<Cart|undefined>}
   */
  async updateCart(cart) {
    const userId = cart.owner;
    const user = await this.userController.getById(userId);
    cart.totalPrice = this.getTotalPrice(cart);
    user.cart = cart;

    const updated = await UserRepository.updateUser(user);
    return updated.cart;
  }

  /**
   * Updates the purchase.
   *
   * @param {Purchase} purchase
   * @returns {Promise<Purchase|undefined>}
   */
  async updatePurchase(purchase) {
    return UserRepository.updatePurchase(purchase);
  }

  /**
   * Approves a purchase.
   *
   * @param {string} purchaseId
   * @returns {Promise<Purchase|undefined>}
   */
  async approvePurchase(purchaseId) {
    const purchase = await this.getPurchaseById(purchaseId);
    purchase.status = "Odobreno";
    const updated = await this.updatePurchase(purchase);
    return updated;
  }

  /**
   * Rejects a purchase.
   *
   * @param {string} purchaseId
   * @param {string} rejectMessage
   * @returns {Promise<Purchase|undefined>}
   */
  async rejectPurchase(purchaseId, rejectMessage) {
    const purchase = await this.getPurchaseById(purchaseId);
    purchase.status = "Odbijeno";
    purchase.rejectMessage = rejectMessage;
    const updated = await this.updatePurchase(purchase);
    return updated;
  }

  /**
   * Adds a chocolate to the cart.
   *
   * @param {Cart} cart
   * @param {Chocolate} chocolate
   * @param {number} amount
   * @returns {promise<Cart|undefined>}
   */
  async addToCart(cart, chocolate, amount) {
    chocolate = await this.addAmountToChocolate(chocolate, -amount);
    const found = cart.cartItems.find(
      (item) => item.chocolate._id === chocolate._id
    );
    if (found) {
      found.amount += amount;
    } else cart.cartItems.push({ chocolate: chocolate, amount: amount });

    const factory = await this.factoryController.getFactoryById(
      chocolate._factoryId
    );
    cart.factory = factory._id;
    cart.factoryName = factory.name;
    cart.totalPrice = this.getTotalPrice(cart);
    return await this.updateCart(cart);
  }

  /**
   * Removes item from cart.
   *
   * @param {Cart} cart
   * @param {Chocolate} chocolate
   * @returns {Promise<Cart|undefined>}
   */
  async removeFromCart(cart, chocolate) {
    const found = cart.cartItems.find((i) => i.chocolate._id === chocolate._id);
    const amount = found.amount;

    cart.cartItems = cart.cartItems.filter((i) => i !== found);
    cart.totalPrice = this.getTotalPrice(cart);
    await this.addAmountToChocolate(chocolate, amount);

    return await this.updateCart(cart);
  }

  /**
   * Adds to the chocolate available amount;
   *
   * @param {Chocolate} chocolate
   * @param {number} amount
   * @returns {Promise<Chocolate|undefined>}
   */
  async addAmountToChocolate(chocolate, amount) {
    chocolate.availableAmount += amount;
    const updated = this.factoryController.updateChocolate(chocolate);
    return updated;
  }

  /**
   * Updates a cart.
   *
   * @param {Cart} cart
   * @returns {Promise<Purchase|undefined>}
   */
  async makePurchase(cart) {
    const userId = cart.owner;
    const user = await this.userController.getById(userId);

    const purchase = Purchase.fromUser(user);
    purchase._purchaseId = `P_${(+new Date()).toString(36).slice(-8)}`;
    user.points += this.getPointsForPrice(user.points);
    user.customerType = CustomerType.fromPoints(user.points);
    purchase.totalPrice = purchase.totalPrice * this.getDiscountForUser(user);

    for (const item of purchase.chocolates) {
      const chocolate = item.chocolate;
      const amount = item.amount;
      const newAmount = chocolate.availableAmount - amount;
      await this.factoryController.changeAmountOfChocolate(
        chocolate._id,
        newAmount
      );
    }

    user.purchases.push(purchase);
    user.cart = null;

    const updated = await UserRepository.updateUser(user);
    return updated ? purchase : undefined;
  }

  /**
   * Cancels a purchase.
   *
   * @param {string} purchaseId
   * @returns {Promise<Purchase|undefined>}
   */
  async cancelPurchase(purchaseId) {
    const purchase = await this.getPurchaseById(purchaseId);

    purchase.status = "Otkazano";
    const updatedPurchase = await UserRepository.updatePurchase(purchase);

    const owner = await UserRepository.getById(updatedPurchase._owner);
    const lostPoints = Math.floor((purchase.totalPrice / 1000) * 133 * 4);
    owner.points = lostPoints >= owner.points ? 0 : owner.points - lostPoints;
    owner.customerType = CustomerType.fromPoints(owner.points);
    const updatedOwner = await UserRepository.updateUser(owner);

    return updatedPurchase;
  }

  /**
   * Updates a purchase.
   *
   * @param {Purchase} purchase
   * @returns {promise<Purchase|undefined>}
   */
  updatePurchase(purchase) {
    return UserRepository.updatePurchase(purchase);
  }

  /**
   * Calculates a total price of the cart
   *
   * @param {Cart} cart
   * @returns {number}
   */
  getTotalPrice(cart) {
    let total = 0;
    cart.cartItems.forEach((item) => {
      total += item.chocolate.price * item.amount;
    });

    return total;
  }

  /**
   * Calculates the amount of points.
   *
   * @param {number} totalPrice
   * @returns {number}
   */
  getPointsForPrice(totalPrice) {
    return Math.floor((totalPrice / 1000) * 133);
  }

  /**
   * Gets discount multiplier for user.
   *
   * @param {User} user
   */
  getDiscountForUser(user) {
    const customerType = user.customerType ? user.customerType.name : "";
    switch (customerType) {
      case "Bronzani":
        return 0.95;
      case "Srebrni":
        return 0.9;
      case "Zlatni":
        return 0.8;
      default:
        return 1;
    }
  }
}

export default PurchaseController;
