import ChocolateFactoryRepository from "../repositories/ChocolateFactoryRepository.js";
import Comment from "../models/Comment.js";
import ChocolateFactoryController from "./ChocolateFactoryController.js";
import UserController from "./UserController.js";

class CommentController {
  constructor() {
    this.factoryController = new ChocolateFactoryController();
    this.userController = new UserController();
  }

  /**
   * Gets a comment by id.
   *
   * @param {string} commentId
   * @returns {Promise<Comment|undefined>}
   */
  getCommentById(commentId) {
    return ChocolateFactoryRepository.getCommentById(commentId);
  }

  /**
   * Checks if user is allowed to comment.
   *
   * @param {string} userId
   * @param {string} factoryId
   * @returns {Promise<boolean>}
   */
  async isAllowedToPostComment(userId, factoryId) {
    const user = await this.userController.getById(userId);
    const factory = await this.factoryController.getFactoryById(factoryId);

    const purchases = user.purchases;
    const comments = factory.comments;

    const userHasPurchase = purchases.find(
      (p) => p._owner === userId && p.status === "Odobreno"
    );
    const userHasCommented = comments.find((c) => c.user === user.username);

    return userHasPurchase && !userHasCommented;
  }

  /**
   * Posts a comment.
   *
   * @param {string} username
   * @param {string} text
   * @param {number} grade
   * @param {string} factoryId
   * @returns {Promise<Comment|undefined>}
   */
  async makeComment(username, text, grade, factoryId) {
    const comment = new Comment(
      null,
      username,
      factoryId,
      text,
      grade,
      "Obrada"
    );

    const newComment = await this.saveComment(comment);
    const factory = await this.factoryController.getFactoryById(factoryId);
    await this.factoryController.updateFactoryRating(factory);

    return newComment;
  }

  /**
   * Approves a comment.
   *
   * @param {string} commentId
   */
  async approveComment(commentId) {
    const comment = await this.getCommentById(commentId);
    comment.status = "Odobren";
    return await this.updateComment(comment);
  }

  async rejectComment(commentId) {
    const comment = await this.getCommentById(commentId);
    comment.status = "Odbijen";
    return await this.updateComment(comment);
  }

  /**
   * Updates a comment.
   *
   * @param {Comment} comment
   * @returns {promise<Comment|undefined>}
   */
  updateComment(comment) {
    return ChocolateFactoryRepository.updateComment(comment);
  }

  /**
   *
   * @param {Comment} comment
   * @returns {Promise<Comment|undefined>}
   */
  async saveComment(comment) {
    const factory = await ChocolateFactoryRepository.getFactoryById(
      comment.factory
    );
    return ChocolateFactoryRepository.saveComment(comment, factory);
  }
}

export default CommentController;
