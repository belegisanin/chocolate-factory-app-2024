import ChocolateFactoryRepository from "../repositories/ChocolateFactoryRepository.js";
import ChocolateFactory from "../models/ChocolateFactory.js";
import Chocolate from "../models/Chocolate.js";

class ChocolateFactoryController {
  /**
   * Gets all ChocolateFactory objects.
   *
   * @returns {Promise<ChocolateFactory[]>} Promise of an Array of all ChocolateFactory objects.
   */
  async getAllFactories() {
    return ChocolateFactoryRepository.getAllFactories();
  }

  /**
   * Retrieves a chocolate factory by its ID.
   *
   * @param {string} factoryId - Id of a chocolate factory.
   * @returns {Promise<ChocolateFactory|undefined>} Promise of a ChocolateFactory object if exists, undefinded otherwise.
   */
  async getFactoryById(factoryId) {
    return ChocolateFactoryRepository.getFactoryById(factoryId);
  }

  /**
   * Retrieves a chocolate by its ID.
   *
   * @param {string} chocolateId
   * @returns {Promise<Chocolate|undefined>}
   */
  async getChocolateById(chocolateId) {
    return ChocolateFactoryRepository.getChocolateById(chocolateId);
  }

  /**
   * Adds a new Chocolate Factory.
   *
   * @param {ChocolateFactory} factory - Factory to be added.
   * @returns {Promise<ChocolateFactory|undefined>}
   */
  async addFactory(factory) {
    factory.logo = `/images/${factory.logo}`;
    factory.chocolates = [];
    factory.status = "Radi";
    factory.rating = 0;
    factory.deleted = false;
    return ChocolateFactoryRepository.saveFactory(factory);
  }

  /**
   * Adds a chocolate object to a specified factory.
   *
   * @param {Chocolate} chocolate - A chocolate object to be added.
   * @param {string} factoryId - Id of a factory to which the chocolate will be added.
   * @returns {Promise<Chocolate|undefined>} Promise of a Chocolate object if operation is successful, undefined otherwise.
   */
  async addChocolate(chocolate, factoryId) {
    let factory = await this.getFactoryById(factoryId);
    return await ChocolateFactoryRepository.saveChocolate(chocolate, factory);
  }

  /**
   * Updates a ChocolateFactory.
   *
   * Field _id of ChocolateFactory has to be set.
   *
   * @param {ChocolateFactory} factory - An ChocolateFactory object to be updated.
   * @returns {Promise<boolean>} A promise that resolves to true if the factory is successfully updated, otherwise false.
   */
  async updateFactory(factory) {
    return ChocolateFactoryRepository.updateFactory(factory);
  }

  /**
   * Updates a Chocolate.
   * Fields _id and _factoryId have to be set.
   *
   * @param {Chocolate} chocolate - A Chocolate object.
   * @returns {Promise<Chocolate|undefined>}
   */
  async updateChocolate(chocolate) {
    return ChocolateFactoryRepository.updateChocolate(chocolate);
  }

  /**
   * Logically deletes a factory.
   *
   * @param {ChocolateFactory} factory
   * @returns {Promise<boolean>}
   */
  async deleteFactory(factory) {
    return ChocolateFactoryRepository.deleteFactory(factory);
  }

  /**
   * Logically deletes a chocolate.
   *
   * @param {Chocolate} chocolate
   * @returns {Promise<boolean>}
   */
  async deleteChocolate(chocolate) {
    return ChocolateFactoryRepository.deleteChocolate(chocolate);
  }

  async changeAmountOfChocolate(chocolateId, newAmount) {
    let chocolate = await this.getChocolateById(chocolateId);
    chocolate.availableAmount = newAmount;
    chocolate.status =
      chocolate.availableAmount > 0 ? "Ima na stanju" : "Nema na stanju";

    return this.updateChocolate(chocolate);
  }

  /**
   * Calculates an updated rating.
   *
   * @param {ChocolateFactory} factory
   * @returns {Promise<ChocolateFactory|undefined>}
   */
  updateFactoryRating(factory) {
    const grades = factory.comments.map((c) => c.grade);
    const average = grades.reduce((a, b) => a + b, 0) / grades.length;
    factory.rating = Math.round(average * 100) / 100;
    return this.updateFactory(factory);
  }
}

export default ChocolateFactoryController;
